<?php

/**
 * Duplicates controller.
 *
 * @category   apps
 * @package    imap
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/imap/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Duplicates controller.
 *
 * @category   apps
 * @package    imap
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/imap/
 */

class Duplicates extends ClearOS_Controller
{
    /**
     * Duplicates overview.
     *
     * @return view
     */

    function index()
    {
        // Bail if accounts system not up and running
        //-------------------------------------------

        $this->load->module('accounts/status');

        if ($this->status->unhappy())
            return;

        // Load libraries
        //---------------

        $this->lang->load('imap');
        $this->load->library('imap/Cyrus');

        // Load view data
        //---------------

        try {
            $running = $this->cyrus->get_running_state();
            // Bail if not running for now
            if (!$running)
                return;
            $data['list'] = $this->cyrus->get_duplicate_deliver_list();
        } catch (\Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load views
        //-----------

        $this->page->view_form('imap/duplicates', $data, lang('imap_duplicate_delivery'), $options);
    }

    /**
     * Group edit members view.
     *
     * @return view
     */

    function edit_members()
    {
        $this->_handle_members('edit');
    }

    /**
     * Group members view.
     *
     * @return view
     */

    function view_members()
    {
        $this->_handle_members('view');
    }

    ///////////////////////////////////////////////////////////////////////////////
    // P R I V A T E
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Group common view/edit members form handler.
     *
     * @param string $form_type form type (add, edit or view)
     *
     * @return view
     */

    function _handle_members($form_type)
    {
        // Load libraries
        //---------------

        $this->lang->load('imap');
        $this->load->library('imap/Cyrus');

        // Handle form submit
        //-------------------

        if ($this->input->post('submit')) {
            try {
                $users = array();
                // A period is not permitted as key, so translate it into a colon
                foreach ($this->input->post('users') as $user => $state)
                    $users[] = preg_replace('/:/', '.', $user);
                
                $this->cyrus->set_duplicate_deliver_list($users);
                $this->page->set_status_updated();

                redirect('/imap');
            } catch (\Exception $e) {
                $this->page->view_exception($e);
                return;
            }
        }

        // Load the view data 
        //------------------- 

        try {
            $data['mode'] = $form_type;
            $data['list'] = $this->cyrus->get_duplicate_deliver_list();
        } catch (\Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load the views
        //---------------

        $this->page->view_form('imap/duplicates_members', $data, lang('imap_duplicate_delivery_members'));
    }
}
